// Mad Lib
// Ian Gruender

#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

struct MadLib
{
	string Adjective[2];
	string Noun[2];
	string PluralNoun[2];
	string Game;
	string Verb[2];
};

void PrintMadLib(MadLib& word, ostream& os = cout)
{
	os << "A vacation is when you take a trip to some " << word.Adjective[0] << " place with your " << word.Adjective[1]
		<< " family.\nUsually you go to some place that is near a/an " << word.Noun[0] << " or up on a/an " << word.Noun[1]
		<< ".\nA good vacation place is one where you can ride " << word.PluralNoun[0] << " or play " << word.Game[0] 
		<< " \nor hunting for " << word.PluralNoun[0] << ". I like to spend my time " << word.Verb[0] << " or " << word.Verb[1] << ".\n";
}

int main()
{
	MadLib word;
	string saveoutput;

	for (int i = 0; i < 1; i++)
	{
		cout << "Enter an adjective: ";
		cin >> word.Adjective[0];
		cout << "\nEnter an adjective: ";
		cin >> word.Adjective[1];
		cout << "\nEnter a noun: ";
		cin >> word.Noun[0];
		cout << "\nEnter a noun: ";
		cin >> word.Noun[1];
		cout << "\nEnter a plural noun: ";
		cin >> word.PluralNoun[0];
		cout << "\nEnter a game: ";
		cin >> word.Game[0];
		cout << "\nEnter a plural noun: ";
		cin >> word.PluralNoun[1];
		cout << "\nEnter a verb ending in ING: ";
		cin >> word.Verb[0];
		cout << "\nEnter a verb ending in ING: ";
		cin >> word.Verb[1];
	}
	PrintMadLib(word);

	cout << "Would you like to save output file? (y/n): ";
	cin >> saveoutput;
	if (saveoutput == "y") 
	{
		string path = "madlib.txt";
		ofstream ofs(path);
		PrintMadLib(word, ofs);
		ofs.close();
		cout << "\nMad Lib has been saved to madlib.txt";
	}
	else
	{
		cout << "\nPress any key to exit";
	}


	(void)_getch();
	return 0;
}